package com.sp.model;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TransactionTest {
	private List<Integer>VendeurList;
	private List<Integer> CarteList;
	
	@Before
	public void setUp() {
		System.out.println("[BEFORE TEST] -- Add User to test");
		VendeurList = new ArrayList<Integer>();
		CarteList = new ArrayList<Integer>();
		
		for(int i = 0; i<4;i++) {
			VendeurList.add(i);
			CarteList.add(i);
		}

	}

	@After
	public void tearDown() {
		System.out.println("[AFTER TEST] -- CLEAN hero list");
		VendeurList = null;
		CarteList = null;
	}
	

	
	@Test
	public void createTransaction() {
		int size = VendeurList.size();
		for(int i = 0; i< size; i++) {
			Transaction h=new Transaction( VendeurList.get(i),CarteList.get(i) );
			assertTrue(h.getIdVendeur() == CarteList.get(i));
			assertTrue(h.getIdCarte() == CarteList.get(i));
		}
	}
	
	@Test
	public void displayTransaction() {
		int idCarte = 1;
		int idVendeur = 1;
		Transaction h=new Transaction(idVendeur,idCarte);
		String expectedResult="[ Transaction: "+0+
				", Id Vendeur: "+idVendeur +
				", Id Carte: "+idCarte +" ]";
		assertTrue(h.toString().equals(expectedResult));
	}
}
