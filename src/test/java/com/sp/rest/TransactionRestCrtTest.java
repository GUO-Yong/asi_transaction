package com.sp.rest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.sp.model.Transaction;
import com.sp.service.TransactionService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = TransactionRestCtr.class)
public class TransactionRestCrtTest {
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private TransactionService tService;

	Transaction mockTransaction=new Transaction(1,1);
	
	@Test
	public void retrieveTransaction() throws Exception {
		Mockito.when(
				tService.getTransactionById(Mockito.anyInt())
				).thenReturn(mockTransaction);
				

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/transactions/50").accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder)
			      .andExpect(MockMvcResultMatchers.jsonPath("$.idCarte").value(1))
			      .andReturn();

	}

}
