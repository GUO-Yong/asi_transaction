package com.sp.service;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.model.Transaction;
import com.sp.repository.TransactionRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = TransactionService.class)
public class TransactionServiceTest {

	@Autowired
	private TransactionService tService;

	@MockBean
	private TransactionRepository tRepo;
	
	Transaction tempTransaction=new Transaction(1,1);
	
	@Test
	public void getTransaction() {
		Mockito.when(
				tRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tempTransaction));
		Transaction tInfo=tService.getTransactionById(45);
		assertTrue(tInfo.toString().equals(tempTransaction.toString()));
	}
	
}
